# Websocket Communicator

## Setup

- `$ npm install` (make sure you're authed to Alula NPM registry)
- `$ npm start`
- Type out JSON parseable messages to send to the system, then hit enter. For example:
  - ```
    {"channel": "rest.events", "subscribe": {"resourceType": "devices"}}
    ```
