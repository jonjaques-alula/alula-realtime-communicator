import { AlulaAPI } from "@alula-internal/universal-js-client";

export const api = new AlulaAPI({
  baseUrl: process.env.BASE_URL!,
  clientId: process.env.CLIENT_ID!,
  clientSecret: process.env.CLIENT_SECRET!
});
