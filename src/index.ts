require("dotenv").config();
const { USERNAME, PASSWORD } = process.env;

import { api } from "./api";

main().catch(err => console.log(err));

async function main() {
  await api.login(USERNAME!, PASSWORD!);

  const socket = await api.client.socket();

  socket.on("json", (data: any) => {
    // console.log(data);
    if (data.event && data.event.resource && data.event.resource.data) {
      console.log(data.event.resource.data);
    }
  });

  socket.on("error", err => {
    console.error("error", err);
  });

  process.stdin.on("data", data => {
    try {
      const msg = JSON.parse(data);
      socket.ws.send(JSON.stringify(msg));
    } catch (err) {
      console.error("parse error", err);
    }
  });
}
